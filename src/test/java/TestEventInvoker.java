import java.util.Date;

import com.lenovo.commonevent.Event;
import com.lenovo.commonevent.EventService;
import com.lenovo.commonevent.IEventConsumedCallback;

/**
 * Project:      CommonEvent
 * FileName:     TestEventInvoker.java
 * @Description: TODO
 * @author:      ligh4
 * @version      V1.0 
 * Createdate:   2015年3月16日 下午6:03:47
 * Copyright:    Copyright(C) 2014-2015
 * Company       Lenovo LTD.
 * All rights Reserved, Designed By Lenovo CIC.
 */

/**
 * 类 TestEventInvoker 的实现描述：TODO 类实现描述
 * 
 * @author ligh4 2015年3月16日下午6:03:47
 */
public class TestEventInvoker implements IEventConsumedCallback {

    /**
     * @author ligh4 2015年3月16日下午6:04:02
     */
    @SuppressWarnings("deprecation")
    @Override
    public void onEventFinished(Event event, Object result) {
        System.out.println("Event callback " + event.getId() + " at "
                + ((Date) result).toLocaleString());

    }

    public static void main(String args[]) throws Exception {

        EventService.init(null);

        EventService.registerEventHandler(TestEvent.class.getSimpleName(), new TestEventHandler());

        for (int i = 0; i < 10; i++) {
            TestEvent event = new TestEvent();
            EventService.fireEvent(event, new TestEventInvoker());
        }

        Thread.sleep(5000);

        EventService.stop();
    }
}
