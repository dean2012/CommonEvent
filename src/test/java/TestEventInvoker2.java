import java.io.FileInputStream;
import java.io.IOException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

import com.lenovo.commonevent.EventService;

/**
 * Project:      CommonEvent
 * FileName:     TestEventInvoker2.java
 * @Description: TODO
 * @author:      ligh4
 * @version      V1.0 
 * Createdate:   2015年3月18日 下午3:36:14
 * Copyright:    Copyright(C) 2014-2015
 * Company       Lenovo LTD.
 * All rights Reserved, Designed By Lenovo CIC.
 */

/**
 * 类 TestEventInvoker2 的实现描述：TODO 类实现描述
 * 
 * @author ligh4 2015年3月18日下午3:36:14
 */
public class TestEventInvoker2 {

    public static void main(String args[]) throws Exception {

        String config_file = "/handlers.properties";
        FileInputStream inputStream = null;
        Properties props = new Properties();
        try {
            //inputStream = new FileInputStream(config_file);   
            props.load(TestEventInvoker2.class.getResourceAsStream(config_file));

        } catch (InvalidPropertiesFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        EventService.init(props);

        for (int i = 0; i < 10; i++) {
            TestEvent event = new TestEvent();
            EventService.fireEvent(event);
        }

        Thread.sleep(5000);

        EventService.stop();
    }
}
